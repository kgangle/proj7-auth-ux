# Laptop Service
import pymongo
import sys
from pymongo import MongoClient
from flask import Flask, request
from flask_restful import Resource, Api
from itsdangerous import (TimedJSONWebSignatureSerializer \
                                  as Serializer, BadSignature, \
                                  SignatureExpired)
import time
from passlib.apps import custom_app_context as pwd_context

database = MongoClient('db', 27017)
db = database.tododb

# Instantiate the app
app = Flask(__name__)
api = Api(app)




class List_All(Resource):
	#this Resource returns all times in the database, in a JSON format

	#if there is no 'top'  argument, we set a limit 
	limit = 99
	if len(sys.argv) > 1:
		limit = request.args.get('top')
		if limit == None:
			limit = 99

	def get(self):

		dates = db.tododb.find({'open_times',pymongo.ASCENDING}, {'close_times',pymongo.ASCENDING}).limit(limit)
		_dates = [date for date in dates]

		return flask_jsonify(result=_dates)

api.add_resource(List_All, '/listAll')	

class List_All_json(Resource):
	#this Resource returns all times in the database, in a JSON format

	#if there is no 'top'  argument, we set a limit 
	limit = 99
	if len(sys.argv) > 1:
		limit = request.args.get('top')
		if limit == None:
			limit = 99

	def get(self):

		dates = db.tododb.find({'open_times',pymongo.ASCENDING}, {'close_times',pymongo.ASCENDING}).limit(limit)
		_dates = [date for date in dates]

		return flask_jsonify(result=_dates)

api.add_resource(List_All_json, '/listAll/json')

class List_Open_Only(Resource):
	#this Resource returns only the Open Times, in a JSON format	

	#if there is no 'top'  argument, we set a limit 
	limit = 99
	if len(sys.argv) > 1:
		limit = request.args.get('top')
		if limit == None:
			limit = 99

	def get(self):

		dates = db.tododb.find({'open_times',pymongo.ASCENDING}).limit(limit)
		_dates = [date for date in dates]

		return flask_jsonify(result=_dates)

api.add_resource(List_Open_Only, '/listOpenOnly')

class List_Open_Only_json(Resource):
	#this Resource returns only the Open Times, in a JSON format	

	#if there is no 'top'  argument, we set a limit 
	limit = 99
	if len(sys.argv) > 1:
		limit = request.args.get('top')
		if limit == None:
			limit = 99

	def get(self):

		dates = db.tododb.find({'open_times',pymongo.ASCENDING}).limit(limit)
		_dates = [date for date in dates]

		return flask_jsonify(result=_dates)

api.add_resource(List_Open_Only_json, '/listOpenOnly/json')

class List_Close_Only(Resource):
	#this Resource returns only the Close Times, in a JSON format

	#if there is no 'top'  argument, we set a limit 
	limit = 99
	if len(sys.argv) > 1:
		limit = request.args.get('top')
		if limit == None:
			limit = 99

	def get(self):

		dates = db.tododb.find({'close_times',pymongo.ASCENDING}).limit(limit)
		_dates = [date for date in dates]

		return flask_jsonify(result=_dates)

api.add_resource(List_Close_Only, '/listCloseOnly')

class List_Close_Only_json(Resource):
	#this Resource returns only the Close Times, in a JSON format

	#if there is no 'top'  argument, we set a limit 
	limit = 99
	if len(sys.argv) > 1:
		limit = request.args.get('top')
		if limit == None:
			limit = 99

	def get(self):

		dates = db.tododb.find({'close_times',pymongo.ASCENDING}).limit(limit)
		_dates = [date for date in dates]

		return flask_jsonify(result=_dates)

api.add_resource(List_Close_Only_json, '/listCloseOnly/json')

class List_All_csv(Resource):
	#This Resource returns all of the times in the database, in a csv format

	#if there is no 'top'  argument, we set a limit 
	limit = 99
	if len(sys.argv) > 1:
		limit = request.args.get('top')
		if limit == None:
			limit = 99

	def get(self):

		dates = db.tododb.find({'open_times',pymongo.ASCENDING}, {'close_times',pymongo.ASCENDING}).limit(limit)
		return_string = ''

		for date in dates:
			return_string += date['open_times'] + ", "
			return_string += date['close_times'] + ", "
		return return_string

api.add_resource(List_All_csv, '/listAll/csv')

class List_Close_csv(Resource):
	#This Resource returns only the Close Times, in a csv format

	#if there is no 'top'  argument, we set a limit 
	limit = 99
	if len(sys.argv) > 1:
		limit = request.args.get('top')
		if limit == None:
			limit = 99

	def get(self):

		dates = db.tododb.find({'close_times',pymongo.ASCENDING}).limit(limit)
		return_string = ''

		for date in dates:
			return_string += date['close_times'] + ", "
		return return_string

api.add_resource(List_Close_csv, '/listCloseOnly/csv')

class List_Open_csv(Resource):
	#This Resource returns only the Open Times, in a csv format

	#if there is no 'top'  argument, we set a limit 
	limit = 99
	if len(sys.argv) > 1:
		limit = request.args.get('top')
		if limit == None:
			limit = 99

	def get(self):

		dates = db.tododb.find({'open_times',pymongo.ASCENDING}).limit(limit)
		return_string = ''

		for date in dates:
			return_string += date['open_times'] + ", "
			return_string += date['close_times'] + ", "
		return return_string

api.add_resource(List_Open_csv, '/listOpenOnly/csv')

#Authorization area
def create_user(Resource):
	def post(self):
		parser = reqparse.RequestParser()
		parser.add_argument('name', required = True)
		parser.add_argument('pw', required = True)
		args = parser.parse_args()
		name = args['name']
		pw = args['pw']
		hash_pw = hash_password(pw)
		post_id = collection.users.insert_one({'username':name,'password':hash_pw})
		ID=str(post_id.inserted_id)

		return{'success':'created','username':name,'location':ID}, 201

api.add_resource(registerUser, '/api/register')

class get_token(Resource):
	def get(self):
		auth = request.headers.get('Authorization')
		if auth == None:
			return {'Unauthorized':'No Authorization found in header'},401

		cred = auth.split(' ')
		decode_cred = b64decode(cred[1]).decode()
		user = decode_cred.spilt(':')
		name = user[0]
		pw = user[1]

		person = collection.users.find_one({'username':name})
		if person == None:
			return {'Unauthorized':'No user'}, 401
		if not verify_password(pw,document['password']):
			return {'Unauthorized':'Incorrect pw'}, 401

		token = generate_auth_token(expiration = 3600)
		return {'token':token.decode(), 'duration':3600}, 200

api.add_resource(getToken, '/api/token')

def generate_auth_token(expiration=3600):
   # s = Serializer(app.config['SECRET_KEY'], expires_in=expiration)
   s = Serializer('test1234@#$', expires_in=expiration)
   # pass index of user
   return s.dumps({'id': 1})

def verify_auth_token(token):
    s = Serializer('test1234@#$')
    try:
        data = s.loads(token)
    except SignatureExpired:
        return None    # valid token, but expired
    except BadSignature:
        return None    # invalid token
    return "Success"


def hash_password(password):
    return pwd_context.encrypt(password)

def verify_password(password, hashVal):
    return pwd_context.verify(password, hashVal)

# Run the application
if __name__ == '__main__':
    app.run(host='0.0.0.0', port=80, debug=True)
